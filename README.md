### Modificar la ruta del backend API_BASE el archivo config.js
```
utils/config.js
```

### Arrancar el proyecto con:

```bash
npm run dev
# or
yarn dev
```

### abrir [http://localhost:3000](http://localhost:3000) en el navegador web

### luego dar click sobre Cidenet S.A.S que enviara al login si se subio el .sql puede iniciar sesión con el correo
```
daniel.caballero@cidenet.com.co
```