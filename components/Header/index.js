import config from "../../utils/config";
import Link from "next/link";

export default function Header() {
  return (
    <>
      <header>
        <Link href="/admin">
          <a>{config.APP_NAME}</a>
        </Link>
        <div>
          <link
            rel="stylesheet"
            href="https://cdnjs.cloudflare.com/ajax/libs/twitter-bootstrap/3.3.7/css/bootstrap.min.css"
          />
          <link
            rel="stylesheet"
            href="https://cdnjs.cloudflare.com/ajax/libs/material-design-iconic-font/2.2.0/css/material-design-iconic-font.min.css"
          />

          <link rel="stylesheet" href="/css/style.css" />
        </div>
      </header>
    </>
  );
}
