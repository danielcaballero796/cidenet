import { useDispatch } from "react-redux";
import { singOut } from "../../redux/actions";

export default function Sidebar() {
  const dispatch = useDispatch();
  return (
    <>
      <ul className="nav">
        <li>
          <a
            onClick={() => {
              dispatch(singOut());
              window.location = "/login";
            }}
          >
            Cerrar Sesión
          </a>
        </li>
      </ul>
    </>
  );
}
