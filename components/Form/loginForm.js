import { useState } from "react";
import styles from "../../styles/modules/admin/LoginForm.module.css";
import config from "../../utils/config";
import axios from "axios";
import Swal from "sweetalert2";

/* redux */
import { useDispatch, useSelector } from "react-redux";
import { setUser, setToken } from "../../redux/actions";

export default function LoginForm() {
  const state = useSelector((state) => state);
  const dispatch = useDispatch();
  const [mail, setMail] = useState("");

  const handlerChangeMail = (e) => {
    setMail(e.target.value);
  };

  /**
   * I'm sending a post request to the server, and if the response is successful, I'm redirecting the
   * user to the admin page.
   * @param e - the event that is being handled
   */
  const handleSubmit = async (e) => {
    e.preventDefault();

    let data = {
      mail: e.target.mail.value,
    };

    const info = await axios
      .post(config.API_BASE + "auth", data, {
        headers: {
          "Content-type": "application/json; charset=UTF-8",
        },
      })
      .then(function (response) {
        return response.data;
      })
      .catch(function (error) {
        Swal.fire({
          position: "center",
          icon: "error",
          title: "Ocurrio un error al iniciar sesion",
          showConfirmButton: false,
          timer: 1500,
        });
      });

    if (info != undefined) {
      dispatch(setToken(info.data));
      dispatch(setUser(info.data));
      window.location = "/admin";
    }
  };

  if (state.token) {
    window.location.replace("/admin");
  } else {
    return (
      <>
        <main className={styles.body}>
          <div className={styles.login}>
            <h2 className={styles.login_header}>Inicio de Sesión</h2>
            <form className={styles.login_container} onSubmit={handleSubmit}>
              <p>
                <input
                  name="mail"
                  type="email"
                  placeholder="ejemplo@cidenet.com.co"
                  onChange={handlerChangeMail}
                  value={mail}
                />
              </p>
              <p>
                <input type="submit" value="Iniciar" />
              </p>
            </form>
          </div>
        </main>
      </>
    );
  }
}
