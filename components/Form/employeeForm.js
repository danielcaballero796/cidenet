import { useState } from "react";
import "bootstrap/dist/css/bootstrap.css";
import { Button, Form, FormGroup, Label, Input } from "reactstrap";
import { useAreas } from "../../hooks/useAreas";
import { useCountries } from "../../hooks/useCountries";
import { useTypes } from "../../hooks/useTypes";
import config from "../../utils/config";
import axios from "axios";
import Swal from "sweetalert2";
/* redux */
import { useSelector } from "react-redux";

export default function EmployeeForm(props) {
  const state = useSelector((state) => state);
  const {
    name,
    method,
    myEmployee,
    setTableEmployeeActive,
    setRegisterEmployeeActive,
  } = props;

  const { areas } = useAreas();
  const { countries } = useCountries();
  const { types } = useTypes();

  const [first_name, setFirstName] = useState("");
  const [others_names, setOthersNames] = useState("");
  const [first_surname, setFirstSurname] = useState("");
  const [second_surname, setSecondSurname] = useState("");
  const [identification, setIdentification] = useState("");
  const [entry_date, setEntryDate] = useState("");
  const [id_country, setIdCountry] = useState(-1);
  const [id_type_id, setIdTypeId] = useState(-1);
  const [id_area, setIdArea] = useState(-1);

  /**
   * It takes an event object as an argument, and depending on the name of the input that triggered the
   * event, it will set the corresponding state variable to the value of the input.
   * @param e - event
   */
  const handlerChangeInput = (e) => {
    switch (e.target.name) {
      case "first_surname":
        if (/^[a-zA-Z]*$/.test(e.target.value)) {
          setFirstSurname(e.target.value.toUpperCase());
        }
        break;
      case "second_surname":
        if (/^[a-zA-Z]*$/.test(e.target.value)) {
          setSecondSurname(e.target.value.toUpperCase());
        }
        break;
      case "first_name":
        if (/^[a-zA-Z]*$/.test(e.target.value)) {
          setFirstName(e.target.value.toUpperCase());
        }
        break;
      case "others_names":
        if (/^[a-zA-Z]*$/.test(e.target.value)) {
          setOthersNames(e.target.value.toUpperCase());
        }
        break;
      case "identification":
        if (/^[0-9]*$/.test(e.target.value)) {
          setIdentification(e.target.value);
        }
        break;
      case "entry_date":
        if (
          /^(19|20)(((([02468][048])|([13579][26]))-02-29)|(\d{2})-((02-((0[1-9])|1\d|2[0-8]))|((((0[13456789])|1[012]))-((0[1-9])|((1|2)\d)|30))|(((0[13578])|(1[02]))-31)))$/.test(
            e.target.value
          )
        ) {
          setEntryDate(e.target.value);
        }
        break;
      case "id_country":
        if (/^[0-9]*$/.test(e.target.value)) {
          setIdCountry(e.target.value);
        }
        break;
      case "id_type_id":
        if (/^[0-9]*$/.test(e.target.value)) {
          setIdTypeId(e.target.value);
        }
        break;
      case "id_area":
        if (/^[0-9]*$/.test(e.target.value)) {
          setIdArea(e.target.value);
        }
        break;
      default:
        break;
    }
  };

  /**
   * When the user clicks the button, close the form and open the table of employees
   */
  const handlerChangeClose = (e) => {
    e.preventDefault();
    setTableEmployeeActive(true);
    setRegisterEmployeeActive(false);
  };

  /**
   * when a submit event occurs, it validates whether it's a registration or an update
   * and sends it to the corresponding service.
   * @param e - event
   */
  const handlerSubmit = async (e) => {
    e.preventDefault();

    let data = {
      id: myEmployee.id,
      first_surname: document
        .getElementById("first_surname")
        .value.toUpperCase(),
      second_surname: document
        .getElementById("second_surname")
        .value.toUpperCase(),
      first_name: document.getElementById("first_name").value.toUpperCase(),
      others_names: document.getElementById("others_names").value.toUpperCase(),
      id_country: document.getElementById("id_country").value,
      id_type_id: document.getElementById("id_type_id").value,
      identification: document.getElementById("identification").value,
      entry_date: document.getElementById("entry_date").value,
      id_area: document.getElementById("id_area").value,
    };

    if (method == "put") {
      updateEmployee(data);
    } else if (method == "post") {
      createEmployee(data);
    }
  };

  /**
   * It sends a POST request to the API with the data and the token, if the request is successful, it
   * shows a success message, if not, it shows an error message.
   * @param data JSON with de data
   */
  const createEmployee = async (data) => {
    await axios
      .post(config.API_BASE + "employees", data, {
        headers: {
          "Content-type": "application/json; charset=UTF-8",
          Authorization: state.token,
        },
      })
      .then(function (response) {
        Swal.fire({
          position: "center",
          icon: "success",
          title: response.data.data,
          showConfirmButton: false,
          timer: 1500,
        });
        window.location.replace("/admin");
      })
      .catch(function (error) {
        Swal.fire({
          position: "center",
          icon: "error",
          title: "Ocurrio un error, intentelo nuevamente",
          showConfirmButton: false,
          timer: 1500,
        });
      });
  };

  /**
   * It sends a PUT request to the API with the data and the token, if the request is successful, it
   * shows a success message, if not, it shows an error message.
   * @param data JSON with de data
   */
  const updateEmployee = async (data) => {
    await axios
      .put(config.API_BASE + "employees", data, {
        headers: {
          "Content-type": "application/json; charset=UTF-8",
          Authorization: state.token,
        },
      })
      .then(function (response) {
        Swal.fire({
          position: "center",
          icon: "success",
          title: response.data.data,
          showConfirmButton: false,
          timer: 1500,
        });
        window.location.replace("/admin");
      })
      .catch(function (error) {
        Swal.fire({
          position: "center",
          icon: "error",
          title: "Ocurrio un error, intentelo nuevamente",
          showConfirmButton: false,
          timer: 1500,
        });
      });
  };

  return (
    <>
      <div className="col-md-2"></div>
      <div className="col-md-8">
        <Form>
          <FormGroup>
            <Label for="exampleEmail">Primer Apellido</Label>
            <Input
              id="first_surname"
              name="first_surname"
              placeholder="Primer Apellido"
              type="text"
              onChange={handlerChangeInput}
              value={
                first_surname == "" ? myEmployee.first_surname : first_surname
              }
            />
          </FormGroup>
          <FormGroup>
            <Label for="exampleEmail">Segundo Apellido</Label>
            <Input
              id="second_surname"
              name="second_surname"
              placeholder="Segundo Apellido"
              type="text"
              onChange={handlerChangeInput}
              value={
                second_surname == ""
                  ? myEmployee.second_surname
                  : second_surname
              }
            />
          </FormGroup>
          <FormGroup>
            <Label for="exampleEmail">Primer Nombre</Label>
            <Input
              id="first_name"
              name="first_name"
              placeholder="Primer Nombre"
              type="text"
              onChange={handlerChangeInput}
              value={first_name == "" ? myEmployee.first_name : first_name}
            />
          </FormGroup>
          <FormGroup>
            <Label for="exampleEmail">Otros Nombres</Label>
            <Input
              id="others_names"
              name="others_names"
              placeholder="Otros Nombres"
              type="text"
              onChange={handlerChangeInput}
              value={
                others_names == "" ? myEmployee.others_names : others_names
              }
            />
          </FormGroup>
          <FormGroup>
            <Label for="exampleSelect">Pais</Label>
            <Input
              id="id_country"
              name="id_country"
              type="select"
              onChange={handlerChangeInput}
              value={id_country == -1 ? myEmployee.id_country : id_country}
            >
              <option value="-1">Seleccione un Valor</option>
              {countries.map((country) => {
                return (
                  <option key={country.id} value={country.id}>
                    {country.name}
                  </option>
                );
              })}
            </Input>
          </FormGroup>
          <FormGroup>
            <Label for="exampleSelect">Tipo de Identificación</Label>
            <Input
              id="id_type_id"
              name="id_type_id"
              type="select"
              onChange={handlerChangeInput}
              value={id_type_id == -1 ? myEmployee.id_type_id : id_type_id}
            >
              <option value="-1">Seleccione un Valor</option>
              {types.map((country) => {
                return (
                  <option key={country.id} value={country.id}>
                    {country.name}
                  </option>
                );
              })}
            </Input>
          </FormGroup>
          <FormGroup>
            <Label for="exampleEmail">Número de Identificación</Label>
            <Input
              id="identification"
              name="identification"
              placeholder="Número de Identificación"
              type="number"
              onChange={handlerChangeInput}
              value={
                identification == ""
                  ? myEmployee.identification
                  : identification
              }
            />
          </FormGroup>
          <FormGroup>
            <Label for="exampleEmail">Fecha de Ingreso</Label>
            <Input
              id="entry_date"
              name="entry_date"
              placeholder="with a placeholder"
              type="date"
              onChange={handlerChangeInput}
              value={entry_date == "" ? myEmployee.entry_date : entry_date}
            />
          </FormGroup>
          <FormGroup>
            <Label for="exampleSelect">Area</Label>
            <Input
              id="id_area"
              name="id_area"
              type="select"
              onChange={handlerChangeInput}
              value={id_area == -1 ? myEmployee.id_area : id_area}
            >
              <option value="-1">Seleccione un Valor</option>
              {areas.map((country) => {
                return (
                  <option key={country.id} value={country.id}>
                    {country.name}
                  </option>
                );
              })}
            </Input>
          </FormGroup>

          <div className="row">
            <div className="col-md-10"></div>
            <div className="col-md-1">
              <Button
                className="pull-right"
                color="success"
                onClick={handlerSubmit}
              >
                {name}
              </Button>
            </div>
            <div className="col-md-1">
              <Button
                className="pull-right"
                color="danger"
                onClick={handlerChangeClose}
              >
                Cancelar
              </Button>
            </div>
          </div>
        </Form>
      </div>
      <div className="col-md-2"></div>
    </>
  );
}
