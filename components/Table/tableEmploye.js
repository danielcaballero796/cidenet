import { useState, useMemo } from "react";
import { Input, Button } from "reactstrap";
import DataTable from "react-data-table-component";

function convertArrayOfObjectsToCSV(array) {
  let result;
  const columnDelimiter = ",";
  const lineDelimiter = "\n";
  const keys = [
    "id",
    "first_name",
    "others_names",
    "first_surname",
    "second_surname",
    "id_country",
    "country",
    "id_type_id",
    "type_identification",
    "identification",
    "mail",
    "entry_date",
    "id_area",
    "area",
    "state",
    "register_date",
    "updated_date",
    "is_admin",
  ];

  result = "";
  result += keys.join(columnDelimiter);
  result += lineDelimiter;

  array.forEach((item) => {
    let ctr = 0;
    keys.forEach((key) => {
      if (ctr > 0) result += columnDelimiter;

      result += item[key];

      ctr++;
    });
    result += lineDelimiter;
  });

  return result;
}

function downloadCSV(array) {
  const link = document.createElement("a");
  let csv = convertArrayOfObjectsToCSV(array);
  if (csv == null) return;

  const filename = "export.csv";

  if (!csv.match(/^data:text\/csv/i)) {
    csv = `data:text/csv;charset=utf-8,${csv}`;
  }

  link.setAttribute("href", encodeURI(csv));
  link.setAttribute("download", filename);
  link.click();
}

const Export = ({ onExport, handlerClickCreate }) => {
  return (
    <>
      <Button color="danger" onClick={handlerClickCreate}>
        Crear Nuevo
      </Button>
      <Button color="danger" onClick={(e) => onExport(e.target.value)}>
        Exportar CSV
      </Button>
    </>
  );
};

export default function TableEmployee(props) {
  const {
    handleUpdate,
    handleDelete,
    data,
    setTableEmployeeActive,
    setRegisterEmployeeActive,
    setMethodForm,
    setNameForm,
  } = props;

  const [filterText, setFilterText] = useState("");
  const [resetPaginationToggle, setResetPaginationToggle] = useState(false);

  const handlerClickCreate = () => {
    setMethodForm("post");
    setNameForm("Registrar");
    setTableEmployeeActive(false);
    setRegisterEmployeeActive(true);
  };

  const actionsMemo = useMemo(
    () => (
      <Export
        onExport={() => downloadCSV(data)}
        handlerClickCreate={handlerClickCreate}
      />
    ),
    []
  );

  const columns = [
    {
      name: "Primer Nombre",
      selector: (row) => row.first_name,
      sortable: true,
    },
    {
      name: "Otros Nombres",
      selector: (row) => row.others_names,
      sortable: true,
    },
    {
      name: "Primer Apellido",
      selector: (row) => row.first_surname,
      sortable: true,
    },
    {
      name: "Segundo Apellido",
      selector: (row) => row.second_surname,
      sortable: true,
    },
    {
      name: "Pais",
      selector: (row) => row.country,
      sortable: true,
    },
    {
      name: "Tipo de Id",
      selector: (row) => row.type_identification,
      sortable: true,
    },
    {
      name: "Identificación",
      selector: (row) => row.identification,
      sortable: true,
    },
    {
      name: "Correo",
      selector: (row) => row.mail,
      sortable: true,
    },
    {
      name: "Fecha de Ingreso",
      selector: (row) => row.entry_date,
      sortable: true,
    },
    {
      name: "Area",
      selector: (row) => row.area,
      sortable: true,
    },
    {
      name: "Estado",
      selector: (row) => (row.state == "1" ? "Activo" : "Inactivo"),
      sortable: true,
    },
    {
      name: "Fecha de Registro",
      selector: (row) => row.register_date,
      sortable: true,
    },
    {
      name: "Editar",
      button: true,
      cell: (row) => (
        <a title="Editar">
          <i
            className="zmdi zmdi-border-color"
            data-id={row.id}
            data-first_name={row.first_name}
            data-others_names={row.others_names}
            data-first_surname={row.first_surname}
            data-second_surname={row.second_surname}
            data-id_country={row.id_country}
            data-id_type_id={row.id_type_id}
            data-identification={row.identification}
            data-mail={row.mail}
            data-entry_date={row.entry_date}
            data-id_area={row.id_area}
            data-state={row.state}
            data-register_date={row.register_date}
            onClick={handleUpdate}
          />
        </a>
      ),
    },
    {
      name: "Eliminar",
      button: true,
      cell: (row) => (
        <a title="Eliminar">
          <i
            className="zmdi zmdi-delete"
            data-id={row.id}
            onClick={handleDelete}
          />
        </a>
      ),
    },
  ];

  const filteredItems = data.filter(
    (item) =>
      (item.first_surname &&
        item.first_surname.toUpperCase().includes(filterText.toUpperCase())) ||
      (item.second_surname &&
        item.second_surname.toUpperCase().includes(filterText.toUpperCase())) ||
      (item.first_name &&
        item.first_name.toUpperCase().includes(filterText.toUpperCase())) ||
      (item.others_names &&
        item.others_names.toUpperCase().includes(filterText.toUpperCase())) ||
      (item.type_identification &&
        item.type_identification
          .toUpperCase()
          .includes(filterText.toUpperCase())) ||
      (item.identification &&
        item.identification.toUpperCase().includes(filterText.toUpperCase())) ||
      (item.country &&
        item.country.toUpperCase().includes(filterText.toUpperCase())) ||
      (item.mail && item.mail.toUpperCase().includes(filterText.toUpperCase()))
  );

  const FilterComponent = ({ filterText, onClear }) => (
    <>
      <div className="row">
        <div className="col-md-2"></div>
        <div className="col-md-8">
          <Input
            type="text"
            placeholder="Filtros"
            value={filterText}
            onChange={handlerChangeInput}
          />
        </div>
        <div className="col-md-2">
          <Button onClick={onClear}>X</Button>
        </div>
      </div>
    </>
  );

  const handlerChangeInput = (e) => {
    setFilterText(e.target.value.toUpperCase());
  };

  const subHeaderComponentMemo = useMemo(() => {
    const handleClear = () => {
      if (filterText) {
        setResetPaginationToggle(!resetPaginationToggle);
        setFilterText("");
      }
    };

    return <FilterComponent filterText={filterText} onClear={handleClear} />;
  }, [filterText, resetPaginationToggle]);

  return (
    <DataTable
      columns={columns}
      data={filteredItems}
      actions={actionsMemo}
      pagination
      paginationResetDefaultPage={resetPaginationToggle}
      subHeader
      subHeaderComponent={subHeaderComponentMemo}
      persistTableHead
    />
  );
}
