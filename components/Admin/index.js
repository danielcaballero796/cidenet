import { useState, useEffect } from "react";
import config from "../../utils/config";
/* Components */
import Header from "../../components/Header";
import Sidebar from "../../components/Sidebar";
import TableEmployee from "../../components/Table/tableEmploye";
import EmployeeForm from "../../components/Form/EmployeeForm";
import { useEmployees } from "../../hooks/useEmployees";
/* styles */
import styles from "../../styles/modules/admin/Admin.module.css";
import Swal from "sweetalert2";
import axios from "axios";

/* redux */
import { useSelector } from "react-redux";

export default function Admin() {
  const state = useSelector((state) => state);
  const [myEmployee, setMyEmployee] = useState({});
  const [methodForm, setMethodForm] = useState("post");
  const [nameForm, setNameForm] = useState("Registrar");
  const [tableEmployeeActive, setTableEmployeeActive] = useState(true);
  const [registerEmployeeActive, setRegisterEmployeeActive] = useState(false);
  const { employees } = useEmployees();

  /**
   * It takes the data from the table and puts it in the update form
   * @param e - is the event that is triggered when the button is clicked
   */
  function handleUpdate(e) {
    let employee = {
      id: e.target.getAttribute("data-id"),
      first_name: e.target.getAttribute("data-first_name"),
      others_names: e.target.getAttribute("data-others_names"),
      first_surname: e.target.getAttribute("data-first_surname"),
      second_surname: e.target.getAttribute("data-second_surname"),
      id_country: e.target.getAttribute("data-id_country"),
      id_type_id: e.target.getAttribute("data-id_type_id"),
      identification: e.target.getAttribute("data-identification"),
      mail: e.target.getAttribute("data-mail"),
      entry_date: e.target.getAttribute("data-entry_date"),
      id_area: e.target.getAttribute("data-id_area"),
      state: e.target.getAttribute("data-state"),
      register_date: e.target.getAttribute("data-register_date"),
    };

    setMyEmployee(employee);
    setMethodForm("put");
    setNameForm("Actualizar");
    setTableEmployeeActive(false);
    setRegisterEmployeeActive(true);
  }

  /**
   * It takes the data from the table and puts it in the form for deleted.
   * @param e - is the event that is triggered when the button is clicked
   */
  async function handleDelete(e) {
    let data = {
      id: parseInt(e.target.getAttribute("data-id")),
    };

    Swal.fire({
      title: "Está seguro de que desea eliminar el empleado?",
      showDenyButton: false,
      showCancelButton: true,
      confirmButtonText: "Si",
      cancelButtonText: "No",
    }).then((result) => {
      if (result.isConfirmed) {
        deleteEmployee(data);
      }
    });
  }

  /**
   * It sends a DELETE request to the API with the data and the token, if the request is successful, it
   * shows a success message, if not, it shows an error message.
   * @param data JSON with de data
   */
  const deleteEmployee = async (data) => {
    await axios
      .delete(config.API_BASE + "employees", {
        headers: {
          "Content-type": "application/json; charset=UTF-8",
          Authorization: state.token,
        },
        data,
      })
      .then(function (response) {
        Swal.fire({
          position: "center",
          icon: "success",
          title: response.data.data,
          showConfirmButton: false,
          timer: 1500,
        });
        window.location.replace("/admin");
      })
      .catch(function (error) {
        Swal.fire({
          position: "center",
          icon: "error",
          title: "Ocurrio un error, intentelo nuevamente",
          showConfirmButton: false,
          timer: 1500,
        });
      });
  };

  if (!state.token) {
    window.location.replace("/login");
  } else {
    return (
      <>
        <div id="viewport">
          <div id="sidebar">
            <Header />
            <Sidebar />
          </div>

          <div id="content">
            <nav className="navbar navbar-default">
              <div className="container-fluid">
                <ul className="nav navbar-nav navbar-right"></ul>
              </div>
            </nav>
            {/* Body */}
            <div className="container-fluid">
              {tableEmployeeActive ? (
                <>
                  <h1 className={styles.title}>Listado de Empleados</h1>
                  <TableEmployee
                    data={employees}
                    handleUpdate={handleUpdate}
                    handleDelete={handleDelete}
                    setMethodForm={setMethodForm}
                    setNameForm={setNameForm}
                    setTableEmployeeActive={setTableEmployeeActive}
                    setRegisterEmployeeActive={setRegisterEmployeeActive}
                  />
                </>
              ) : registerEmployeeActive ? (
                <>
                  <h1 className={styles.title}>Formulario de Empleados</h1>
                  <EmployeeForm
                    name={nameForm}
                    method={methodForm}
                    myEmployee={myEmployee}
                    setTableEmployeeActive={setTableEmployeeActive}
                    setRegisterEmployeeActive={setRegisterEmployeeActive}
                  />
                </>
              ) : (
                <></>
              )}
            </div>
          </div>
        </div>
      </>
    );
  }
}
