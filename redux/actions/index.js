export const setUser = (payload) => ({
  type: "@SET_USER",
  payload,
});

export const setToken = (payload) => ({
  type: "@SET_TOKEN",
  payload,
});

export const singOut = (payload) => ({
  type: "@SING_OUT",
  payload,
});
