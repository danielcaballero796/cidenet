const initialState = {
  token: false,
  user: {},
};

const reducer = (state = initialState, action) => {
  switch (action.type) {
    case "@SET_USER":
      return {
        ...state,
        user: action.payload,
      };
    case "@SET_TOKEN":
      return {
        ...state,
        token: action.payload,
      };
    case "@SING_OUT":
      return {
        token: false,
        user: {},
      };
    default:
      return state;
  }
};

export default reducer;
