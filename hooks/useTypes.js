import { useState, useEffect } from "react";
import config from "../utils/config";
import axios from "axios";

/**
 * It returns a promise that resolves to the data property of the response object.
 * @returns An array of objects.
 */
async function getTypes() {
  return await axios
    .get(config.API_BASE + "typesId", {
      headers: {
        "Content-type": "application/json; charset=UTF-8",
      },
    })
    .then(function (response) {
      return response.data;
    })
    .catch(function (error) {
      console.log(error);
    });
}

export const useTypes = () => {
  const [types, setTypes] = useState([]);
  let type = getTypes();

  useEffect(() => {
    type
      .then(function (response) {
        setTypes(response.data);
      })
      .catch(function (error) {
        console.log(error);
      });
  }, []);

  return { types };
};
