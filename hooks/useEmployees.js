import { useState, useEffect } from "react";
import config from "../utils/config";
import axios from "axios";
/* redux */
import { useSelector } from "react-redux";

/**
 * It returns a promise that resolves to the data property of the response object.
 * @returns An array of objects.
 */
async function getEmployees() {
  const state = useSelector((state) => state);
  return await axios
    .get(config.API_BASE + "employees", {
      headers: {
        "Content-type": "application/json; charset=UTF-8",
        Authorization: state.token,
      },
    })
    .then(function (response) {
      return response.data;
    })
    .catch(function (error) {
      console.log(error);
    });
}

export const useEmployees = () => {
  const [employees, setEmployees] = useState([]);
  let employee = getEmployees();

  useEffect(() => {
    employee
      .then(function (response) {
        setEmployees(response.data);
      })
      .catch(function (error) {
        console.log(error);
      });
  }, []);

  return { employees };
};
