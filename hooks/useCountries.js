import { useState, useEffect } from "react";
import config from "../utils/config";
import axios from "axios";

/**
 * It returns a promise that resolves to the data property of the response object.
 * @returns An array of objects.
 */
async function getCountries() {
  return await axios
    .get(config.API_BASE + "countries", {
      headers: {
        "Content-type": "application/json; charset=UTF-8",
      },
    })
    .then(function (response) {
      return response.data;
    })
    .catch(function (error) {
      console.log(error);
    });
}

export const useCountries = () => {
  const [countries, setCountries] = useState([]);
  let country = getCountries();

  useEffect(() => {
    country
      .then(function (response) {
        setCountries(response.data);
      })
      .catch(function (error) {
        console.log(error);
      });
  }, []);

  return { countries };
};
