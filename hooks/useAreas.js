import { useState, useEffect } from "react";
import config from "../utils/config";
import axios from "axios";

/**
 * It returns a promise that resolves to the data property of the response object.
 * @returns An array of objects.
 */
async function getAreas() {
  return await axios
    .get(config.API_BASE + "areas", {
      headers: {
        "Content-type": "application/json; charset=UTF-8",
      },
    })
    .then(function (response) {
      return response.data;
    })
    .catch(function (error) {
      console.log(error);
    });
}

export const useAreas = () => {
  const [areas, setAreas] = useState([]);
  let area = getAreas();

  useEffect(() => {
    area
      .then(function (response) {
        setAreas(response.data);
      })
      .catch(function (error) {
        console.log(error);
      });
  }, []);

  return { areas };
};
